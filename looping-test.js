const {Builder, By, Key, until} = require('selenium-webdriver');
const Q = require("q");
const readlineSync = require('readline-sync');
const fs = require('fs');
const dateFormat = require('dateformat');

// Configuration
//....

// State variables for loops
var contractIndex = 0;
var trialCounter = 0;

// create export folder if not present
var exportDir = './export';

if (!fs.existsSync(exportDir)){
    fs.mkdirSync(exportDir);
}

promiseWhile(function () { return true }, function () {

  if(contractIndex < contractNumbers.length - 1 ){
      contractIndex++;
  } else {
      contractIndex = 0;
  }

  // Define variables for this test
  let contractNr = contractNumbers[contractIndex];
  let datetime = new Date();
  trialCounter++;

  let prefix = dateFormat(datetime, "yyyy-mm-dd'T'HH'h'MM'm'ss's'") + '_contract-' + contractNr + '_' + environement ;

  // Log seprator
  console.log("")
  console.log("=== " + trialCounter + " === " + prefix + " ===============================")


  // build browser
  var driver = new Builder()
    //.forBrowser('chrome')
      .forBrowser('firefox')
      .build();


  // Test begin here
  return driver.manage().timeouts().implicitlyWait(25000)
  .then(_ => driver.manage().timeouts().pageLoadTimeout(25000) )
  .then(_ => driver.manage().timeouts().setScriptTimeout(0) )
  .then(_ => driver.quit())

}).then(function () {
    console.log("done");
}).done();



// Utils

function writeScreenshoToDisk(image, filename, testCaseId){
  fs.writeFile( exportDir + '/' + filename + '_screenshot' + '_' + testCaseId + '.png', image, 'base64', function(err) {
    if (err != null){
        console.log(err);
    }
  });
}

function writeSessionInfoToDisk(prefix, sessionInfo){
  fs.writeFile( exportDir + '/' + prefix + '_metainfo' + '.txt', sessionInfo, 'utf8' , function(err) {
      if (err != null){
          console.log(err);
      }
    })
}

// `condition` is a function that returns a boolean
// `body` is a function that returns a promise
// returns a promise for the completion of the loop
function promiseWhile(condition, body) {
  var done = Q.defer();

  function loop() {
      // When the result of calling `condition` is no longer true, we are
      // done.
      if (!condition()) return done.resolve();
      // Use `when`, in case `body` does not return a promise.
      // When it completes loop again otherwise, if it fails, reject the
      // done promise
      Q.when(body(), loop, done.reject);
  }

  // Start running the loop in the next tick so that this function is
  // completely async. It would be unexpected if `body` was called
  // synchronously the first time.
  Q.nextTick(loop);

  // The promise
  return done.promise;
}
