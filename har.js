const {Builder, By, Key, until, Capabilities} = require('selenium-webdriver');
const seleniumProxy = require('selenium-webdriver/proxy');
var MobProxy = require('browsermob-proxy-api');
const fs = require('fs');


// Create proxy api, proxy server itself has to be running already
var proxy = new MobProxy({'host':'localhost', 'port': '8080'});

// start listening on port 10800: 
proxy.startPort(10800, function(err, data) {
    // start new HAR report 
    proxy.createHAR(10800, { 'initialPageRef': 'google.com' });

	var driver = new Builder()
		.withCapabilities(Capabilities.chrome())
		.setProxy(seleniumProxy.manual({http: 'localhost:10800', https: 'localhost:10800'}))
		.build()

	driver.manage().timeouts().implicitlyWait(25000)
  		.then(_ => driver.manage().timeouts().pageLoadTimeout(25000) )
  		.then(_ => driver.manage().timeouts().setScriptTimeout(0) )
  		.then(_ => driver.get('https://www.google.com') )
		// use the line below instead to check actual proxy configuration
		//driver.get("chrome://net-internals/#proxy").then(_ => driver.sleep(5000))
		.then(_ => driver.get('https://www.google.com'))
		.then(_ => driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN))
		.then(_ => driver.quit())
		.then(_ => {
			proxy.getHAR(10800, function(err, data) {
				let filename = "stuff.har"
				console.log("Writing har to file: " + filename);
				fs.writeFileSync('./' + filename, data, 'utf8');
			});
		})
		.then(_ => proxy.stopPort(10800, function(err, data) { if(err != null) console.log(err) }));

});