#!/usr/bin/env bash

# Start browsermob-proxy
./browsermob-proxy-2.1.4/bin/browsermob-proxy -port 8080

# start selenium test in other terminal, e.g.
# node looping-test.js
# node har.js
